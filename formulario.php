<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
<script type="text/javascript">
    function showContent() {
        element = document.getElementById("accesory");
        check = document.getElementById("cbox1");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }
</script>
</head>
<body>
	<form action="generate_rsv.php" method="POST" >
<h3>Fecha de Reserva</h3>
                                <section>
                                    <label for="week">Selecciona el dia o la temporada que reservaras</label>
                                    <div>                                        
                                        <div class="row">                                        
                                            <div class="col-md-4 offset-md-2">
                                                <h5>Fecha de inicio *</h5>
                                                <div class="input-group">
                                                    <input type="date" class="form-control" name="datepicker-autoclose-inicio" id="datepicker-autoclose-inicio" placeholder="mm/dd/yyyy">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>                                          
                                    </div>
                                    <p>(*) Obligatorio</p>
                                </section>
                                <h3>Dias</h3>
                                <section>
                                    <label for="week">Selecciona los dias que reservaras</label>
                                    <select id="week" name="week" class="select2 form-control m-t-15 required" multiple="multiple" style="height: 36px;width: 100%;" placeholder="Lunes, Martes, etc...">                                        
                                        <optgroup label="Dias de la semana">
                                            <option value="mon">LUNES</option>
                                            <option value="tue">MARTES</option>
                                            <option value="wed">MIÉRCOLES</option>
                                            <option value="thu">JUEVES</option>
                                            <option value="fri">VIERNES</option>
                                            <option value="sat">SÁBADO</option>
                                        </optgroup>                                        
                                    </select>
                                    <p>(*) Obligatorio</p>                                  
                                </section>
                                <div>
                                    <section>
                                       <div class="col-md-3">
                                        <h5>Hora de inicio *</h5>
                                             <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                             <input for="startHour" type="text" name="time" class="form-control" value="07:00">
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                    </div>
                                    </section>
                                </div>
                                        <section>
                                            <div class="col-md-3">
                                                <h5>Hora de finalización *</h5>
                                                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                                    <input for="finishHour" type="text" name="timeOut" class="form-control" value="09:00">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            </section>
                                 <h3>Accesorios</h3>
                                 <section>
                                    <div>
                                    <h5>¿Necesitas un accesorio adicional?</h5>
                                        <label><input type="checkbox" id="cbox1" value="first_checkbox" onchange="javascript:showContent()"  />Agregar Accesorio</label>
                                    </div>
                                 <div id="accesory" style="display: none">
                                         <select name="accesor" id="accesor" class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;">
                                    <?php 
                                      include 'get_accesorios.php';
                                      ?>
                                    </select>
                                </div>
                                </section>
                                <h3>Datos personales</h3>
                                <section>
                                    <div>
                                        <label>Reservacion Generada A Nombre De:</label>
                                    <select name="users" id="users" class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;">
                                    <?php 
                                      include 'getUsers.php';
                                      ?>
                                    </select>
                                    </div>
                                    <p>(*) Obligatorio</p>
                                </section>
                            </div>
                                           

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <input type="submit" value="Reservar"class="btn btn-primary"/>
		</form>

</body>
</html>
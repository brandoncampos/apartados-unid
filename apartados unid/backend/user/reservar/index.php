<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<include data_rsv.php>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 id="user_info" class="page-title" data-user="<?php echo $u_id ?>"> <small><?php echo $u_id ?>(Docente)</small></h4>
            <!--<div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div>-->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Sales Cards  -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="./?mod=2" class="card card-hover" data-toggle="modal" data-target="#reservationModalproyector" data-whatever="Reserva de proyector" data-type="proyector">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="fas fa-chart-line"></i></h1>
                    <h6 class="text-white">Reservar Proyector</h6>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalauditorio" data-whatever="Reserva de auditorio" data-type="auditorio">
                <div class="box bg-success text-center">
                    <h1 class="font-light text-white"><i class="fas fa-chalkboard-teacher"></i></h1>
                    <h6 class="text-white">Reservar Auditorio</h6>
                </div>
            </a>
        </div>
            <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalradio" data-whatever="Reserva de cabina de radio" data-type="cabina">
                <div class="box bg-warning text-center">
                    <h1 class="font-light text-white"><i class="fas fa-microphone-alt"></i></h1>
                    <h6 class="text-white">Reservar cabina de radio</h6>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalothers" data-whatever="Reserva de otros recursos" data-type="otros">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                    <h6 class="text-white">Reservar otros</h6>
                </div>
            </a>
        </div>                                        
    </div>
    <!-- ============================================================== -->
    <!-- Start Modal Content -->
    <!-- ============================================================== -->


    <div class="modal fade bd-example-modal-lg" data-target=".bd-example-modal-lg" id="reservationModalradio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="reservationModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body wizard-content"> 
                    <h3>llena los campos para completar la reserva</h3>                       
                        <h6 class="card-subtitle"></h6>
                        <form id="example-form" action="data_rsv.php" method="post" class="m-t-40">
                            <div>
                                <section>
                                    <label for="week">Seleccione el dia de la reserva *</label>
                                    <select id="week" class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;" placeholder="Lunes, Martes, etc...">                                        
                                        <optgroup label="Dias de la semana">
                                            <option value="mon">LUNES</option>
                                            <option value="tue">MARTES</option>
                                            <option value="wed">MIÉRCOLES</option>
                                            <option value="thu">JUEVES</option>
                                            <option value="fri">VIERNES</option>
                                            <option value="sat">SÁBADO</option>
                                        </optgroup>                                        
                                    </select>
                                    <p>(*) Obligatorio</p>                                  
                                </section>
                                <h3>Hora De Reserva</h3>
                                <section>
                                    <div>
                                        <div>
                                            <h5>Todas las reservaciones tienen 1:30Hrs de duracion, a partir de la hora que selecciones correra el tiempo</h5>
                                        </div>
                                       <label for="week">Horas</label>
             <select name="time" id="select_time"class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;" placeholder="Lunes, Martes, etc...">
            <option value="7">7:00</option>
            <option value="7-med">7:30</option>
            <option value="8">8:00</option>
            <option value="8-med">8:30</option>
            <option value="9">9:00</option>
            <option value="9-med">9:30</option>
            <option value="10">10:00</option>
            <option value="10-med">10:30</option>
            <option value="11">11:00</option>
            <option value="11-med">11:30</option>
            <option value="12">12:00</option>
            <option value="12-med">12:30</option>
            <option value="1">1:00</option>
            <option value="1-med">1:30</option>
            <option value="2">2:00</option>
        </select>
        <button>Agregar</button>
                                    <p>(*) Obligatorio</p>                                                                    
                                </section>
                                <h3>Confirma tu reservacion</h3>
                                <section>
                                    <div>
                                        <div class="card-body">                                            
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Reserva a nombre de:</label>
                                                <div class="col-sm-6">

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Periodo de reservacion</label>
                                                <div class="col-sm-6">
                                                <h5>07:00AM</h5>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class=" col-sm-3 text-right control-label col-form-label">Equipo a reservar</label>
                                                <div class="col-sm-6">
                                                    
                                                </div>
                                            </div>                                                                                        
                                        </div>
                                    </div>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" disabled>Consultar Disponibilidad</button>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
    <!-- ============================================================== --> 
    <!-- End Modal Content -->
    <!-- ============================================================== -->
    
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<head>
    <script type="text/javascript">
    function showContent() {
        element = document.getElementById("accesory");
        element2 = document.getElementById("accesory_table");
        check = document.getElementById("cbox1");
        if (check.checked) {
            element.style.display='block';
            element2.style.display='block';
        }
        else {
            element.style.display='none';
            element2.style.display='none';
        }
    }
</script>

<script type="text/javascript">
  function genera_tabla() {
   // Obtener la referencia del elemento body
  var body = document.getElementsByTagName("section")[3];
 
  // Crea un elemento <table> y un elemento <tbody>
  var tabla   = document.createElement("table");
  var tblBody = document.createElement("tbody");
  var th = document.createElement("th");
  var th =document.createTextNode("prueba");

 
  // Crea las celdas
  for (var i = 0; i < 1; i++) {
    // Crea las hileras de la tabla
    var hilera = document.createElement("tr");
    
 
    for (var j = 0; j < 1; j++) {
      // Crea un elemento <td> y un nodo de texto, haz que el nodo de
      // texto sea el contenido de <td>, ubica el elemento <td> al final
      // de la hilera de la tabla


      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(document.getElementById('week').value);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(document.getElementById('select_time').value);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);
      

    }
 
    // agrega la hilera al final de la tabla (al final del elemento tblbody)
    tblBody.appendChild(hilera);
  }
 
  // posiciona el <tbody> debajo del elemento <table>
  tabla.appendChild(tblBody);
  // appends <table> into <body>
  body.appendChild(tabla);
  // modifica el atributo "border" de la tabla y lo fija a "2";
  tabla.setAttribute("border", "1");
}
</script>


<include data_rsv.php>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 id="user_info" class="page-title" data-user="<?php echo $u_id ?>"> <small><?php echo $u_id ?>(Docente)</small></h4>
            <!--<div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div>-->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Sales Cards  -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="./?mod=2" class="card card-hover" data-toggle="modal" data-target="#reservationModalproyector" data-whatever="Reserva de proyector" data-type="proyector">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="fas fa-chart-line"></i></h1>
                    <h6 class="text-white">Reservar Proyector</h6>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalauditorio" data-whatever="Reserva de auditorio" data-type="auditorio">
                <div class="box bg-success text-center">
                    <h1 class="font-light text-white"><i class="fas fa-chalkboard-teacher"></i></h1>
                    <h6 class="text-white">Reservar Auditorio</h6>
                </div>
            </a>
        </div>
            <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalradio" data-whatever="Reserva de cabina de radio" data-type="cabina">
                <div class="box bg-warning text-center">
                    <h1 class="font-light text-white"><i class="fas fa-microphone-alt"></i></h1>
                    <h6 class="text-white">Reservar cabina de radio</h6>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a class="card card-hover" data-toggle="modal" data-target="#reservationModalothers" data-whatever="Reserva de otros recursos" data-type="otros">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                    <h6 class="text-white">Reservar otros</h6>
                </div>
            </a>
        </div>                                        
    </div>
    <!-- ============================================================== -->
    <!-- Start Modal Content -->
    <!-- ============================================================== -->


    <div class="modal fade bd-example-modal-lg" data-target=".bd-example-modal-lg" id="reservationModalradio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="reservationModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body wizard-content"> 
                    <h3>llena los campos para completar la reserva</h3>                       
                        <h6 class="card-subtitle"></h6>
                        <form id="example-form" href="#" method="post" class="m-t-40">
                        <div>
                            <div>
                                <h3>Dia a reservar</h3>
                                <section>
                                    <label for="week">Seleccione el dia de la reserva *</label>
                                    <select id="week" class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;" placeholder="Lunes, Martes, etc...">                                        
                                        <optgroup label="Dias de la semana">
                                            <option value="Lunes">LUNES</option>
                                            <option value="Martes">MARTES</option>
                                            <option value="Miercoles">MIÉRCOLES</option>
                                            <option value="Jueves">JUEVES</option>
                                            <option value="Viernes">VIERNES</option>
                                            <option value="sabado">SÁBADO</option>
                                        </optgroup>                                        
                                    </select>
                                    <p>(*) Obligatorio</p>                                  
                                </section>
                            </div>
                                <h3>Hora De Reserva</h3>
                                <div>
                                    <section>                     
                                        <div>
                                            <h5>Todas las reservaciones tienen 1:30Hrs de duracion, a partir de la hora que selecciones correra el tiempo de la reserva</h5>
                                        </div>
                                       <label for="week">Horas</label>
                                         <select name="time" id="select_time"class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;">
                                                 <option value="7:00">7:00</option>
                                                 <option value="7:30">7:30</option>
                                                 <option value="8:00">8:00</option>
                                                 <option value="8:30">8:30</option>
                                                 <option value="9:00">9:00</option>
                                                 <option value="9:30">9:30</option>
                                                 <option value="10:00">10:00</option>
                                                 <option value="10:30">10:30</option>
                                                 <option value="11:00">11:00</option>
                                                 <option value="11:30">11:30</option>
                                                 <option value="12:00">12:00</option>
                                                 <option value="12:30">12:30</option>
                                                 <option value="1:00">1:00</option>
                                                 <option value="1:30">1:30</option>
                                                 <option value="2:00">2:00</option>
                                        </select>
                                        <p>(*) Obligatorio</p>   

                                    </section>
                                </div> 
               
                                 <h3>Accesorios</h3>
                                 <h5>¿Necesitas un accesorio adicional?</h5>
                                 <label><input type="checkbox" id="cbox1" value="first_checkbox" onchange="javascript:showContent()"  />Agregar Accesorio</label>
                                 <div>
                                  <section>
                                <br>
                               <!-- muestra el contro de accesorios (conflicto con un textbox del template) -->
                               
                             
                               <div id="accesory" style="display: none">
                                    <select name="accesor" id="accesor"class="select2 form-control m-t-1¨5" required style="height: 36px;width: 100%;">
                                    <option>Cable HDMI</option>
                                    <option>Bocinas USB</option>
                                    <option>Extension de corriente</option>
                                    <option>microfono</option>
                                </div>
                                </section>
                              </div> 
                              <input type="button"  value="Agregar Datos" onclick="genera_tabla();" class="btn btn-primary"/>
                              <br>
                              <br>

                                <h3>Confirma tu reservacion</h3>
                                <section>
                                    <div>
                                        <div class="card-body">
                                         <table>
                                             <thead>
                                                 <th>Dia de Reserva</th>
                                                 <br>
                                                 <th>Hora de inicio</th>
                                                 <br>
                                                 <th>Hora de termino</th>
                                                 <br></br>
                                                 <th id="accesory_table" style="display: none" >Accesorios</th>
                                             </thead>

                                             <tbody>
                                                 
                                             </tbody>   
                                         </table>                                          
                                          


                                        </div>
                                    </div>
                                </section>
                          </div>  
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" >Consultar Disponibilidad</button>
                    </div>
                </div>
                </div>
    <!-- ============================================================== --> 
    <!-- End Modal Content -->
    <!-- ============================================================== -->

    </head>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
